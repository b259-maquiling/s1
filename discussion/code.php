<?php

// echo "Hello";

$name = 'John Smith';
$email = 'johnsmith@gmail.com';

define('PI', 3.1416);
define('student', "Joseph");

$student = 'Joseph';
$PI = 3.1416;

$state = 'New York';
$country = 'United States of America';
$address = $state . ',' . $country;
$address = "$state , $country";

$age = 18;
$headCount = 16;

$grade = 98.2;
$distanceInKilometers = 1245.89;

$hasTravelledAbroad = false;
$haveSymptoms = true;

$spouse = null;
$middleName = null;

$grades = array(98.7, 92.1, 90.2, 94.6);
$animals = ["Dog", "Cat", "Chicken"];

$gradesObj = (object)[
    'firstGrading' =>  98.7,
    'secondGrading' => 92.1,
    'thirdGrading' => 90.2,
    'fourthGrading' => 94.6
];

$personObj = (object)[
    "fullName" =>  "John Smith",
    "isMarried" =>  false,
    "age"  =>  18,
    "address" => (object)[
        "state" => "New York",
        "country" => "United States of America"
    ]
];

$x = 250;
$y = 120;

$isLegalAge = true;
$isRegistered = false;

function getFullName($fisrtName, $middleInitial, $lastName){
    return "$lastName, $fisrtName $middleInitial";
}

function determineTyphoonIntensity($windSpeed){
    if($windSpeed < 30) {
        return "Not a typhoon yet.";
    } else if($windSpeed <= 61){
        return 'Tropical depression detected';
    } else if($windSpeed >= 62 && $windSpeed <= 88){
        return 'Tropical storm detected';
    } else if($windSpeed >= 89 && $windSpeed <=  117){
        return 'Severe tropical storm detected';
    } else {
        return 'Typhoon detected';
    }
}

function isUnderAge($age){
    return ($age < 18 ) ? true : false;
}

function determineComputerUser($computerNumber){
    switch($computerNumber){
        case 1:
            return 'Linus Torvalds';
            break;
        case 2:
            return 'Steve Jobs';
            break;
        case 3:
            return 'Sid Meier';
            break;
        case 4:
            return 'Onel de Guzman';
            break;
        case 5:
            return 'Christian Salvador';
            break;
        default;
            return $computerNumber . ' is out of bounds.';
            break;    
    }
}

function greeting($str){
    try{
        if(gettype($str) === "string"){
            echo $str;
        } else {
            throw new Exception("Oops!");
        }
    } catch(Exception $e){
        echo $e -> getMessage();
    } finally{
        echo 'I did it again!';
    }
    
}